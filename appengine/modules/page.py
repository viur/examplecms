# -*- coding: utf-8 -*-

import logging
import json
from server import internalExposed, exposed, forceSSL, db, request
from server.modules.page import Page


class page(Page):
	# use Caching
	def buildNav(self):
		def buildTree(rootNode, nodeName, nodes):
			res = {
				"name": nodeName,
				"key": rootNode,
				"subnodes": []}
			for node in nodes:
				if node["parententry"] == rootNode:
					res["subnodes"].append(buildTree(str(node.key()), node["name"], nodes))
			return res

		rootNodes = self.getAvailableRootNodes()
		logging.debug("root Nodes: %r", rootNodes)
		rootNode = rootNodes[0]
		nodes = self.viewSkel().all().run(1000)
		nodes.sort(key=lambda x: x["sortindex"])
		logging.debug("child nodes: %r", nodes)
		nav = json.dumps(buildTree(rootNode["key"], rootNode["name"], nodes))
		dbObj = db.Entity("page-cache", name="nav")
		dbObj["res"] = nav
		dbObj.set("res", nav, False)
		db.Put(dbObj)


	def onItemChanged( self, skel ):
		self.buildNav()

	def loadNav(self):
		return db.Query("page-cache").get()["res"]

	@exposed
	def getNavJson(self):
		request.current.get().response.headers.add_header('content-type', 'application/json', charset='utf-8')
		return self.loadNav()

	@internalExposed
	def getNav(self):
		return json.loads(self.loadNav())

	@exposed
	def view( self, *args, **kwargs ):
		logging.debug("page.view: %r, %r", args, kwargs)
		return super(page, self).view(*args, **kwargs)
