# -*- coding: utf-8 -*-
import logging

from datetime import datetime
from server import utils, db
from server.modules.file import File


class file(File):
	adminInfo = {
		"name": u"My Files",
		"handler": "tree.simple.file",
		"icon": "icons/modules/my_files.svg"
	}

	def getAvailableRootNodes(self, name, *args, **kwargs):
		thisuser = utils.getCurrentUser()
		if thisuser:
			repo = self.ensureOwnModulRootNode()
			res = [{"name": _("common files"), "key": str(repo.key())}]
			return res
		return []

	def ensureOwnModulRootNode(self):
		"""
			Ensures that the modul-global rootNode exists.
			@returns: The Node-object (as ndb.Expando)
		"""

		logging.debug("ensureOwnModulRootNode")
		key = "rep_modul_repo"
		return db.GetOrInsert(
					key,
					self.viewLeafSkel().kindName + "_rootNode",
					creationdate=datetime.now(),
					changedate=datetime.now(),
					rootNode=1,
					is_root_node=1,
					name=u"Common Files",
					**{
						"name.idx": u"common files"
					})


file.jinja2 = True
file.xml = True
file.json = True
