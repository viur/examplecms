# -*- coding: utf-8 -*-
from server import exposed,errors,internalExposed
from renders.jinja2 import default as html_default_renderer
from server import session
import os

class index(html_default_renderer):

	@exposed
	def index(self, *args, **kwargs):
		lang =session.current.getLanguage()
		if not lang:
				lang="en"
		raise errors.Redirect("/"+lang+"/home")
		#template = self.getEnv().get_template("index.html")
		#return template.render(start=True)

	@internalExposed
	def listSpecialSites (self,*args,**kwargs):
		path = "html/specialsites"
		res=[]
		for filename in os.listdir( path ):
			if filename.endswith(".html"):
				res.append(filename[:-5].lower())
		return res
index.jinja2 = True
