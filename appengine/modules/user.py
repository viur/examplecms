# -*- coding: utf-8 -*-
from server.modules.user_custom import CustomUser
from skeletons.user import userSkel

class user(CustomUser):
	baseSkel = userSkel
	viewSkel = userSkel

	adminInfo = {
		"name": u"User",
	    "handler": "list",
	    "icon": "icons/modules/users.svg"
	}
