# -*- coding: utf-8 -*-
import StringIO

from server.prototypes.list import List
from server import errors
from server import request
from server import conf


class news(List):
	listTemplate = "news_list"
	viewTemplate = "news_view"
	columns = ["name","creationdate"]

	adminInfo = {"name": "News",
	             "handler": "list.news",  # Which handler to invoke
	             "icon": "icons/modules/news.svg",  # Icon for this modul
	             "previewurls": {"Web": "/{{modul}}/view/{{id}}"},
	             "filter": {"orderby": "name"},
	             "columns": columns,
	}

	def listFilter(self, filter):
		return filter



news.jinja2 = True
