# -*- coding: utf-8 -*-

import logging
import json
from server import internalExposed, exposed, forceSSL, db, request
from server.modules.site import Site
import os, sys


class site(Site):
	@internalExposed
	def list(self,*args,**kwargs):
		path = "html/sites"
		res=[]
		for filename in os.listdir( path ):
			if filename.endswith(".html"):
				res.append(filename[:-5].lower())
		return res