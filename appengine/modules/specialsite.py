# -*- coding: utf-8 -*-
from server.prototypes.singleton import Singleton
import logging
from skeletons.specialsite import specialsiteSkel
from server import exposed,errors,request,db,securitykey


class Specialsite(Singleton):

	def adminInfo(self):
		if (self.__class__.__name__=="Specialsite"):
			return None
		return {"name": self.__class__.__name__.title(),
	             "handler": "singleton",
	             "icon": "icons/modules/"+self.__class__.__name__+".svg"
						}

	def canView(self):
		return True

	def _resolveSkel(self, *args, **kwargs):
		skel=specialsiteSkel()
		return skel.subSkel(self.__class__.__name__)

	def getKey(self):
		return( "%s-modulkey" % self.__class__.__name__ )


	@exposed
	def index(self,*args,**kwargs):
		return self.view()


	@exposed
	def view( self, *args, **kwargs ):
		"""
		Prepares and renders the singleton entry for viewing.

		The function performs several access control checks on the requested entity before it is rendered.

		In the Exampleproject, the Function is manipulated: If there is no db-entry the Function writes an entry with placeholder content.

		.. seealso:: :func:`viewSkel`, :func:`canView`, :func:`onItemViewed`

		:returns: The rendered representation of the entity.

		:raises: :exc:`server.errors.NotFound`, if there is no singleton entry existing, yet.
		:raises: :exc:`server.errors.Unauthorized`, if the current user does not have the required permissions.
		"""
		self.viewTemplate="specialsites/"+self.__class__.__name__
		return super(Specialsite, self).view(*args, **kwargs)


Specialsite.jinja2 = True
