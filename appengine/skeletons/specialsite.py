# -*- coding: utf-8 -*-
from server.bones import *
from server.config import conf
from server.skeleton import Skeleton


class specialsiteSkel(Skeleton):
	kindName = "specialsite"
	default_bannerimage1 = fileBone(	descr=u"Bannerimage",	indexed=True, languages=conf["defaultlangs"] )
	default_headline1=stringBone (descr=u"Headline", languages=conf["defaultlangs"])
	default_descr1 = textBone (descr="Text",searchable=True, languages=conf["defaultlangs"])

	default_bannerimage2 = fileBone(	descr=u"Bannerimage 2",	indexed=True)
	default_headline2=stringBone (descr=u"Headline 2", languages=conf["defaultlangs"])
	default_descr2 = textBone (descr="Text 2",searchable=True, languages=conf["defaultlangs"])

	home_slogan = textBone (descr="Slogan",searchable=True, languages=conf["defaultlangs"])

	imprint_privacy = textBone (descr="Privacy Statement",searchable=True, languages=conf["defaultlangs"])
	imprint_cookies = textBone (descr="Explaniation about cookies",searchable=True, languages=conf["defaultlangs"])

	subSkels = {
		"imprint":["default_descr1","default_headline1","imprint_*"],
		"home":["default_*","home_*"],
		}
