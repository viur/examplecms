# -*- coding: utf-8 -*-

from server.modules.user_custom import userSkel
from server.bones import *
from server import conf

class userSkel(userSkel):
	company = stringBone(descr=u"Company name", required=False, indexed=True, searchable=True)
	firstname = stringBone(descr=u"First name", required=True, indexed=True, searchable=True)
	lastname = stringBone(descr=u"Last name", required=True, indexed=True, searchable=True)
	image = fileBone(descr="picture")

	twitter = stringBone(descr=u"twittername", required=False, searchable=True)