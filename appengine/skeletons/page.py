# -*- coding: utf-8 -*-

from server.modules.page import pageSkel as pSkel
from server.bones import *
from server import conf

class pageSkel(pSkel):
	name = stringBone (descr=u"Name", languages=conf["defaultlangs"])
	descr = textBone (descr=u"Content",searchable=True, languages=conf["defaultlangs"])
	visible = booleanBone (descr=u"visible")
	image = fileBone (descr="image")
	images = fileBone (descr="images", multiple=True)
	author = userBone (descr="author", creationMagic=True)

