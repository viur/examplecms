# -*- coding: utf-8 -*-
from server.bones import *
from server.skeleton import Skeleton

class appconfSkel(Skeleton):
	kindName = "appconf"

	googleanalyticskey=stringBone(descr="Google analytics key")
	# MEMBER_MARKER
