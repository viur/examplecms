# -*- coding: utf-8 -*-
from server.bones import *
from server.skeleton import Skeleton
from server.config import conf


class news(Skeleton):
    kindName = "news"
    name = stringBone(descr=u"System name", searchable=True, required=True, indexed=True, languages=conf["defaultlangs"])
    headline = stringBone(descr=u"headline", searchable=True, required=True, indexed=True, languages=conf["defaultlangs"])
    descr = textBone(descr=u"Content", required=False,searchable=True, languages=conf["defaultlangs"])
    image = fileBone(	descr=u"Banner image",	indexed=True)
    images = fileBone(	descr=u"Gallery",	indexed=True)

